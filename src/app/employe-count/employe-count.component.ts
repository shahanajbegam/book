import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employe-count',
  templateUrl: './employe-count.component.html',
  styleUrls: ['./employe-count.component.css'],

})
export class EmployeCountComponent implements OnInit {

  selectedRadiobuttonValue: string = 'All';

  @Output()
  countRadioButtonSelectionChanged: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  all: number;

  @Input()
  male: number;

  @Input()
  female: number;

  onRadioButtonSelection() {
    this.countRadioButtonSelectionChanged.emit(this.selectedRadiobuttonValue);
    console.log(this.selectedRadiobuttonValue);
  }

  constructor() { }

  ngOnInit() {
  }

}
