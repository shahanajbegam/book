import { Component, Output,OnInit } from '@angular/core';
import { JsonService } from '../other/json.service'
@Component({
  selector: 'app-other',
  template: `
  
  <div *ngIf="jsonData">
      <dynamic-ng-form [data]="jsonData"></dynamic-ng-form>
  </div>
`,
  styleUrls: ['./other.component.css'],

})
export class OtherComponent implements OnInit {

  constructor(private jsonService:JsonService) { }
  jsonData:any="AAAA AAAAAAAAA";
  ngOnInit() {
    this.jsonService.getMyData().subscribe((data) => {
      this.jsonData = data;
     
      console.log(this.jsonData+"jsonDatasssssssssss");
    });

  }
  formImpl(data:Response){
    console.log(data);
  }
}
