import { Injectable } from '@angular/core';
import { IEmployee } from './../employee.interface';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';


@Injectable()
export class JsonService {

  constructor(private _http: Http) { }

  getMyData(): Observable<any> {
    return this._http.get("assets/db.json")
      .map((response: Response) =>{ return response.json();}).catch(this.handleError).catch(this.handleError);
  }

  handleError(error:Response){
   // console.error(error);
    return Observable.throw(error);
  }


}
