import {NgModule} from '@angular/core';
import {UserPreferenceService} from './services/userpreference.service';


@NgModule({
    providers:[UserPreferenceService]
})
export class TestModule{

}