import { Injectable } from '@angular/core';
import { IEmployee } from './../employee.interface';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';


@Injectable()
export class EmployeeService {

  constructor(private _http: Http) { }

  getEmployees(): Observable<IEmployee[]> {
    return this._http.get("http://localhost:8080/getEmplyee")
      .map((response: Response) => <IEmployee[]>response.json()).catch(this.handleError).catch(this.handleError);
  }

  getEmployeeById(code:string): Observable<IEmployee> {
    return this._http.get("http://localhost:8080/getEmplyeeById/"+code)
    .map((response:Response)=><IEmployee>response.json())
    .catch(this.handleError);
     
  }

  handleError(error:Response){
   // console.error(error);
    return Observable.throw(error);
  }


}




// return [
//   { code: 'emp101', name: 'Tom', gender: 'Male', salary: '1000', dob: '12/11//1999' },
//   { code: 'emp102', name: 'Mom', gender: 'Female', salary: '4000', dob: '01/11//1974' },
//   { code: 'emp103', name: 'Lom', gender: 'Female', salary: '2000', dob: '09/01//1987' },
//   { code: 'emp104', name: 'Pom', gender: 'Male', salary: '3000', dob: '12/02//1984' },
//   { code: 'emp105', name: 'Jom', gender: 'Male', salary: '1200', dob: '08/25//1997' },
//   { code: 'emp106', name: 'Jom', gender: 'Male', salary: '7700', dob: '03/25//1997' }

// ];