import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './../services/employee.service';
import { IEmployee } from './../employee.interface'
import {UserPreferenceService} from '../services/userpreference.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],


})
export class EmployeeListComponent implements OnInit {

  selectedEmployeeCountRadiobutton: string = "All";
  employees: IEmployee[];

    statusMessage:string="Loading data...  please wait !";

//private _userPreferenceService:UserPreferenceService;

  // constructor() {
  //   this._userPreferenceService=new UserPreferenceService();
  //  }
  constructor(private _employeeService: EmployeeService,private _userPreferenceService:UserPreferenceService) {
    // this.employees = this._employeeService.getEmployees();
    //this._userPreferenceService=new UserPreferenceService();

  }

  get colour():string{
    return this._userPreferenceService.clourPreference;
  }
  set colour(value:string){
    this._userPreferenceService.clourPreference=value;
  }

  getTotalEmployeesCount(): number {
    return this.employees.length;
  }

  getTotalMaleEmployeesCount(): number {
    return this.employees.filter(e => e.gender === "Male").length;
  }

  getTotalFemaleEmployeesCount(): number {
    return this.employees.filter(e => e.gender === "Female").length;
  }


  onEmployeeCountRadioButtonChange(selectedRadiobuttonValue: string): void {
    this.selectedEmployeeCountRadiobutton = selectedRadiobuttonValue;
  }

  ngOnInit() {

    
    this._employeeService.getEmployees().subscribe((employeeData) => this.employees = employeeData,(error)=>
    {
        this.statusMessage="Problem with the service. please try again..!";
        
    });
    
  }

}

  ongetEmpList(): void {
    this.employees = [
      { code: 'emp101', name: 'Tom', gender: 'Male', salary: '1000', dob: '12/11//1999' },
      { code: 'emp102', name: 'Mom', gender: 'Female', salary: '4000', dob: '12/11//1974' },
      { code: 'emp103', name: 'Lom', gender: 'Female', salary: '2000', dob: '12/11//1987' },
      { code: 'emp104', name: 'Pom', gender: 'Male', salary: '3000', dob: '12/11//1984' },
      { code: 'emp105', name: 'Jom', gender: 'Male', salary: '1200', dob: '12/11//1997' }

    ];

  }

  // trackByEmpCode(index:number,employee :any): string {
  //   return employee.code;
  // }


