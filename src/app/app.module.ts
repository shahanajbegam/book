import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeTitlePipe } from './employee/employeeTitle.pipe';
import { EmployeCountComponent } from './employe-count/employe-count.component'
import { AlertModule } from 'ngx-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OtherComponent } from './other/other.component';
import { EmployeeService } from './services/employee.service';
import { JsonService } from './other/json.service'
import {UserPreferenceService} from './services/userpreference.service';
import {TestModule} from './test.module'
import {MyFormComponent} from './other/dynamic-forms/dynamic.forms.create'
import { DynamicFormService, DynamicFormControlModel, DynamicFormLayout,DynamicFormValidationService } from "@ng-dynamic-forms/core";

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'employee', component: EmployeeListComponent },
  { path: 'employee/:code',component:EmployeeComponent },
  { path: '**', component: OtherComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,MyFormComponent,
    EmployeeListComponent, EmployeeTitlePipe, EmployeCountComponent, HomeComponent, OtherComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule, AlertModule.forRoot(), RouterModule.forRoot(appRoutes),TestModule
  ],
  providers: [EmployeeService,JsonService,DynamicFormService,DynamicFormValidationService],//,UserPreferenceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
