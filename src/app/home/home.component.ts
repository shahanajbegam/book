import { Component, OnInit } from '@angular/core';
import {UserPreferenceService} from '../services/userpreference.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit {

//private _userPreferenceService:UserPreferenceService;

  constructor(private _userPreferenceService:UserPreferenceService) {
   
   // this._userPreferenceService=new UserPreferenceService();
   }

get colour():string{
  return this._userPreferenceService.clourPreference;
}
set colour(value:string){
  this._userPreferenceService.clourPreference=value;
}

  ngOnInit() {
  }

}
