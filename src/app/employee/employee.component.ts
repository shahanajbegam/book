import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { EmployeeService } from './../services/employee.service';
import { IEmployee } from './../employee.interface'

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],

})

export class EmployeeComponent implements OnInit {

  statusMessagge: string="Loading emplyee deatail";

  emp: IEmployee;

  constructor(private _employeeService: EmployeeService,
               private _activatedRoute: ActivatedRoute,
              private _router:Router) {

  }

onBackButtonClick():void{
  this._router.navigate(['/employee']);
}

  ngOnInit() {
    let empCode: string = this._activatedRoute.snapshot.params['code'];
 
    this._employeeService.getEmployeeById(empCode).subscribe(employeeData => 
      {
        if(employeeData==null){
          this.statusMessagge ="Employee not found...!";
        }else{ 
        this.emp = employeeData;
      }
      },
      errror => { this.statusMessagge = "Problem with service. please try again...! " });


  }





  //18--------------------------------------------------------------------------------------------
  // <td>{{emloyee.code | uppercase}}</td>
  // <td>{{emloyee.name}}</td>
  // <td>{{emloyee.gender}}</td>
  // <td>{{emloyee.salary |currency:'USD':true:'1.3-3'}}</td>
  // <td>{{emloyee.dob | date:'dd/MM/y' |uppercase}}</td>  

  //14 chapter---------------------------------------------------------------------------------------------------
  //
  // firstName: string = 'Irfan';
  // lastName: string = 'yala';
  // gender: string = 'Male';
  // age: number = 24;

   showDetails: boolean = false;

   toggleDetails(): void {

  this.showDetails = !this.showDetails;

  }

  // colSpan: String = '2';

  // constructor() { }
  //   <div style="align:center;">
  //   <table>
  //   <thead>
  //     <tr>
  //       <th [attr.colspan]="colSpan">EMPLOYEE DEATILS : </th>

  //     </tr>
  //   </thead>
  //   <tbody>
  //     <tr>
  //       <td>First Name : </td>
  //       <td>{{firstName}}</td>
  //     </tr>
  //     <tr>
  //       <td>last Name : </td>
  //       <td>{{lastName}}</td>
  //     </tr>
  //     <tr *ngIf="showDetails">
  //       <td>Gender : </td>
  //       <td>{{gender}}</td>
  //     </tr>
  //     <tr *ngIf="showDetails">
  //       <td>Age : </td>
  //       <td>{{age}}</td>
  //     </tr>
  //   </tbody>
  // </table>

  // <br/>

  // <button (click)="toggleDetails()">
  //  {{showDetails ? 'Hide ' : 'Show '}} Details
  // </button>
  // </div>
  //14 chapter end------------------------------------------------------------------------------------------------



}
